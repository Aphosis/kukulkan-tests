import pkg_resources
pkg_resources.declare_namespace(__name__)


node_types = {
    entry_point.name: entry_point.load()
    for entry_point
    in pkg_resources.iter_entry_points('kukulkan.nodes')
}


__all__ = node_types.keys()
