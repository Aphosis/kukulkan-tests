import pkg_resources
pkg_resources.declare_namespace(__name__)


gui_types = {
    entry_point.name: entry_point.load()
    for entry_point
    in pkg_resources.iter_entry_points('kukulkan.guis')
}


__all__ = gui_types.keys()
