from setuptools import setup, find_packages


setup(
    name='kukulkan-tests',
    version='0.1.0',
    description='',
    package_dir={'': 'src'},
    include_package_data=True,
    packages=find_packages('src'),
    license='MIT',
    namespace_packages=['kukulkan.attributes', 'kukulkan.nodes'],
    url='https://gitlab.com/Aphosis/kukulkan-tests/',
    download_url='git+https://gitlab.com/Aphosis/kukulkan-tests.git#egg=kukulkan-tests',
)
